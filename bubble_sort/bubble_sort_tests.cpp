#include <gtest/gtest.h>
#include <vector>

#include "bubble_sort.h"

using std::vector;

bool sortAndTest(vector<int>& inputVector) {
    vector<int> perfectlySorted = inputVector;
    std::sort(perfectlySorted.begin(), perfectlySorted.end());

    BubbleSort(inputVector);

    if(perfectlySorted.size() != inputVector.size())
        return false;

    for(size_t i = 0; i < perfectlySorted.size(); ++i)
        if(perfectlySorted[i] != inputVector[i])
            return false;

    return true;
}

TEST(BubbleSort, EmptyVector)
{
    vector<int> testVector {};
    ASSERT_TRUE(sortAndTest(testVector));
}

TEST(BubbleSort, SingleElementVector)
{
    vector<int> testVector {1};
    ASSERT_TRUE(sortAndTest(testVector));
}

TEST(BubbleSort, SameElementVector)
{
    {
        vector<int> testVector {0, 0};
        ASSERT_TRUE(sortAndTest(testVector));
    }

    {
        vector<int> testVector {42, 42, 42};
        ASSERT_TRUE(sortAndTest(testVector));
    }

    {
        vector<int> testVector {-42, -42, -42};
        ASSERT_TRUE(sortAndTest(testVector));
    }

    int intMin = std::numeric_limits<int>::min();
    int intMax = std::numeric_limits<int>::max();

    {
        vector<int> testVector {intMin, intMin, intMin};
        ASSERT_TRUE(sortAndTest(testVector));
    }

    {
        vector<int> testVector {intMax, intMax, intMax};
        ASSERT_TRUE(sortAndTest(testVector));
    }
}

TEST(BubbleSort, SortedVectors)
{
    {
        vector<int> testVector {1, 2, 3};
        ASSERT_TRUE(sortAndTest(testVector));
    }

    {
        vector<int> testVector {3, 2, 1};
        ASSERT_TRUE(sortAndTest(testVector));
    }

    {
        vector<int> testVector {-1, -2, -3};
        ASSERT_TRUE(sortAndTest(testVector));
    }

    {
        vector<int> testVector {-3, -2, -1};
        ASSERT_TRUE(sortAndTest(testVector));
    }
}

TEST(BubbleSort, RandomizedVectors)
{

    srand(42);

    for(int i = 0; i < 1000; ++i) {
        size_t len = rand() % 100;

        vector<int> testVector(len);
        for(size_t i = 0; i < len; ++i)
            testVector.push_back(rand() % 10000 - 5000);

        ASSERT_TRUE(sortAndTest(testVector));
    }
}

TEST(BubbleSort, LargeVector)
{
    const size_t len = 8 * 1024 / 4;

    vector<int> testVector(len);

    for(size_t i = 0; i < len; ++i)
        testVector.push_back(rand() % 10000 - 5000);

    ASSERT_TRUE(sortAndTest(testVector));
}