#ifndef BUBBLE_SORT_TESTS_BUBBLE_SORT_H
#define BUBBLE_SORT_TESTS_BUBBLE_SORT_H

inline void BubbleSort(std::vector<int>& array)
{
    //fix
    if(array.size() <= 1)
        return;

    bool continueFlag = false;
    do
    {
        continueFlag = false;
        for (int i = 0; i < array.size() - 1; ++i)
        {
            if (array[i] > array[i + 1])
            {
                std::swap(array[i], array[i + 1]);
                continueFlag = true;
            }
        }
    } while (continueFlag);
}

#endif //BUBBLE_SORT_TESTS_BUBBLE_SORT_H
