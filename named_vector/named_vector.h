#ifndef APRI_COURSES_MYVECTOR2_H
#define APRI_COURSES_MYVECTOR2_H

#include <vector>
#include <string>
#include <unordered_map>
#include <functional>
#include <unordered_set>
#include <iostream>
#include <type_traits>
#include <memory>

/* unordered_multimap holds object name and index of corresponding element in vector
   while vector keeps object itself with reference to it's name */

template <class T>
class named_vector {
public:
    using reference = std::pair<T&, const std::string&>;
    using const_reference = std::pair<const T&, const std::string&>;
    using pointer = std::pair<T*, const std::string&>;
    using const_pointer = std::pair<T*, const std::string&>;
    using size_type = size_t;
private:
    template <bool is_const = true>
    class const_nonconst_iterator: public std::iterator<std::random_access_iterator_tag, T> {
    private:
        using my_vector_ptr_type = std::conditional_t<
                is_const,
                const named_vector<T>*,
                named_vector<T>*
        >;

        using value_type = std::conditional_t<
                is_const,
                const_reference,
                reference
        >;
    public:
        const_nonconst_iterator(my_vector_ptr_type container, size_t index): m_container(container), m_index(index) { }

        //const(const), nc(nc)
        const_nonconst_iterator(const const_nonconst_iterator& oth): m_container(oth.m_container), m_index(oth.m_index) { }

        const_nonconst_iterator& operator=(const const_nonconst_iterator& oth) {
            m_container = oth.m_container;
            m_index = oth.m_index;

            return *this;
        }

        bool operator==(const const_nonconst_iterator& oth) {
            return (m_index == oth.m_index) && (m_container == oth.m_container);
        }

        bool operator!=(const const_nonconst_iterator& oth) {
            return !(*this == oth);
        }

        value_type operator*() {
            return (*m_container)[m_index];
        }

        const_nonconst_iterator& operator--() { //--it
            --m_index;
            return *this;
        };

        const_nonconst_iterator operator--(int) { //it--
            const const_nonconst_iterator old(*this);
            --(*this);
            return old;
        }

        const_nonconst_iterator& operator++() { //++it
            ++m_index;
            return *this;
        }

        const_nonconst_iterator operator++(int) { //it++
            const const_nonconst_iterator old(*this);
            ++(*this);
            return old;
        }

        const_nonconst_iterator& operator+=(int offset) {
            m_index += offset;
            return *this;
        }

        const_nonconst_iterator& operator-=(int offset) {
            m_index -= offset;
            return *this;
        }

        const_nonconst_iterator operator+(int offset) {
            return const_nonconst_iterator(m_container, m_index + offset);
        }

        const_nonconst_iterator operator-(int offset) {
            return const_nonconst_iterator(m_container, m_index - offset);
        }
    private:
        size_t m_index;
        my_vector_ptr_type m_container;
    };
public:
    using const_iterator = const_nonconst_iterator<true>;
    using iterator = const_nonconst_iterator<false>;

    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using reverse_iterator = std::reverse_iterator<iterator>;

    named_vector(): names(std::make_shared<UnorderedNameIndexSet>()) { };
    named_vector(const named_vector<T>& other)
            : names(other.names),
              storage(other.storage) { }

    void swap(named_vector<T>& other) {
        using std::swap;

        swap(storage, other.storage);
        swap(names, other.names);
    }

    named_vector(named_vector<T>&& other): named_vector() {
        swap(other);
    }

    named_vector& operator=(named_vector<T>&& other) {
        swap(other);

        return *this;
    }

    void push_back(T&& obj, const std::string& name) {
        copy_names();

        auto insertedNameIndexPair = names->emplace(name, storage.size());
        storage.emplace_back(std::move(obj), &insertedNameIndexPair->first);
    }

    void push_back(const T& obj, const std::string& name) {
        copy_names();

        auto insertedNameIndexPair = names->emplace(name, storage.size());
        storage.emplace_back(obj, &insertedNameIndexPair->first);
    }

    std::pair<T&, const std::string&> operator[](size_t index) {
        if (index >= storage.size())
            throw std::out_of_range("Index is out of range");

        auto& value_name = storage[index];

        return std::pair<T&, const std::string&>(value_name.first, *value_name.second);
    }

    std::pair<const T&, const std::string&> operator[](size_t index) const {
        if (index >= storage.size())
            throw std::out_of_range("Index is out of range");

        auto& value_name = storage[index];
        return std::pair<const T&, const std::string&>(value_name.first, *value_name.second);
    }

    const T& operator[](const std::string& name) const {
        const name_index_pair& nameIndex = findElementByName(name);
        return storage[nameIndex.second].first;
    }

    T& operator[](const std::string& name) {
        const name_index_pair& nameIndex = findElementByName(name);
        return storage[nameIndex.second].first;
    }

    const_iterator cbegin() const {
        return const_iterator(this, 0);
    }
    const_iterator cend() const {
        return const_iterator(this, storage.size());
    }

    iterator begin() {
        return iterator(this, 0);
    }
    iterator end() {
        return iterator(this, storage.size());
    }

    reverse_iterator rbegin() {
        return reverse_iterator(end());
    }

    reverse_iterator rend() {
        return reverse_iterator(end());
    }

    const_reverse_iterator crbegin() {
        return reverse_iterator(end());
    }

    const_reverse_iterator crend() {
        return reverse_iterator(end());
    }

    bool empty() {
        return storage.empty();
    }

    size_t size() {
        return storage.size();
    }

    void reserve(size_t reservation) {
        storage.reserve(reservation);
    }

    void clear() {
        names = std::make_shared<UnorderedNameIndexSet>();
        storage.clear();
    }
private:
    using name_index_pair = std::pair<const std::string, size_t>;
    struct value_name_pair {
        T first;
        const std::string* second;

        value_name_pair(T&& first, const std::string* second):
                first(std::move(first)), second(second) { }
        value_name_pair(const T& first, const std::string* second):
                first(first), second(second) { }
    };

    struct name_index_pair_hasher {
        size_t operator()(const name_index_pair& nameIndexPair) const {
            return std::hash<std::string>()(nameIndexPair.first);
        }
    };

    struct name_index_pair_equal_by_name {
        bool operator()(const name_index_pair& a, const name_index_pair& b) const {
            return a.first == b.first;
        }
    };

    using UnorderedNameIndexSet = std::unordered_multiset<
            name_index_pair,
            name_index_pair_hasher,
            name_index_pair_equal_by_name
    >;

    /* Reference counting implemented easier with shared_ptr instead of manual counting, despite
       insignificant overhead of unnecessary CAS operations and weak_counter */

    std::shared_ptr<UnorderedNameIndexSet> names;
    std::vector<value_name_pair> storage;

    const name_index_pair& findElementByName(const std::string& name) const {
        auto nameIndexIt = names->find(name_index_pair(name, 0));

        if(nameIndexIt == names->end())
            throw std::invalid_argument(name + " is not found in the named_vector");

        return *nameIndexIt;
    }

    void copy_names() {
        if(names.use_count() == 1)
            return;

        names = std::make_shared<UnorderedNameIndexSet>(*names);

        //relink vector element names

        for(auto& name_record: *names) {
            size_t el_index = name_record.second;

            storage[el_index].second = &name_record.first;
        }
    }
};

#endif //APRI_COURSES_MYVECTOR2_H
