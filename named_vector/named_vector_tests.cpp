#include <gtest/gtest.h>

#include "named_vector.h"

named_vector<int> LookupsInit() {
    named_vector<int> v;
    v.push_back(5, "five");
    v.push_back(0, "zero");

    return v;
}

//To test both const and non-const operator overloads
template <class V>
void LookupsTest(V& v) {
    ASSERT_EQ(v["five"], 5);
    ASSERT_EQ(v["zero"], 0);

    ASSERT_THROW(v["six"], std::invalid_argument);
}

TEST(NamedVector, Lookups) {
    auto v = LookupsInit();
    const auto& cv = v;

    LookupsTest(v);
    LookupsTest(cv);
}

template <class V>
void AccessByIndexTest(V& v) {
    auto v_0 = v[0];
    auto v_1 = v[1];

    ASSERT_EQ(v_0.second, "five");
    ASSERT_EQ(v_0.first, 5);

    ASSERT_EQ(v_1.second, "zero");
    ASSERT_EQ(v_1.first, 0);

    ASSERT_THROW(v[2], std::out_of_range);
}

TEST(NamedVector, AccessByIndex) {
    auto v = LookupsInit();
    const auto& cv = v;

    AccessByIndexTest(v);
    AccessByIndexTest(cv);
}

TEST(NamedVector, Iterators) {
    auto v = LookupsInit();

    named_vector<int>::iterator it = v.begin();
    named_vector<int>::const_iterator c_it = v.cbegin();

    named_vector<int>::iterator it_copy = it;
    named_vector<int>::const_iterator c_it_copy = c_it;

    ASSERT_TRUE(it == it_copy);
    ASSERT_TRUE(c_it == c_it_copy);

    ASSERT_EQ((*it).first, 5);
    ASSERT_EQ((*it).second, "five");

    ASSERT_EQ((*c_it).first, 5);
    ASSERT_EQ((*c_it).second, "five");

    ++it;
    ++c_it;

    ASSERT_EQ((*it).first, 0);
    ASSERT_EQ((*it).second, "zero");

    ASSERT_EQ((*c_it).first, 0);
    ASSERT_EQ((*c_it).second, "zero");

    ++it;
    ++c_it;

    ASSERT_TRUE(it == v.end());
    ASSERT_TRUE(c_it == v.cend());

    //moving back to the beginning
    it -= 2;
    c_it -= 2;

    ASSERT_TRUE(it == it_copy);
    ASSERT_TRUE(c_it == c_it_copy);

    //again to the ending
    it = it + 2;
    c_it = c_it + 2;

    ASSERT_TRUE(it == v.end());
    ASSERT_TRUE(c_it == v.cend());

    //to v[1]
    it = it - 1;
    c_it = c_it - 1;

    ASSERT_EQ((*it).first, 0);
    ASSERT_EQ((*it).second, "zero");

    ASSERT_EQ((*c_it).first, 0);
    ASSERT_EQ((*c_it).second, "zero");

    //to the ending

    it += 1;
    c_it += 1;

    ASSERT_TRUE(it == v.end());
    ASSERT_TRUE(c_it == v.cend());

    //again to v[1]

    --it;
    --c_it;

    ASSERT_EQ((*it).first, 0);
    ASSERT_EQ((*it).second, "zero");

    ASSERT_EQ((*c_it).first, 0);
    ASSERT_EQ((*c_it).second, "zero");
}

//TooLazyTODO: reverse iterators test

TEST(NamedVector, VectorFunctions) {
    auto v = LookupsInit();

    ASSERT_EQ(v.size(), 2);
    ASSERT_FALSE(v.empty());

    v.clear();

    ASSERT_EQ(v.size(), 0);
    ASSERT_TRUE(v.empty());
}

TEST(NamedVector, CopyMoveTest) {
    auto v = LookupsInit();

    decltype(v) copy = v;

    ASSERT_EQ(v[0].first, copy[0].first);
    ASSERT_EQ(v[0].second, copy[0].second);

    //content of internal vector is suggested to be copied immediately
    ASSERT_NE(&v[0].first, &copy[0].first);

    //while name hash table is shared
    ASSERT_EQ(&v[0].second, &copy[0].second);

    //right to the point of write operation
    int i = 88;
    copy.push_back(i, "88");

    //names are suggested to be copied and relinked with corresponding elements now
    ASSERT_NE(&v[0].second, &copy[0].second);


    //moves
    decltype(v) tmp1(std::move(v));
    decltype(v) tmp2(std::move(copy));

    v = std::move(tmp1);
    copy = std::move(tmp2);

    //recheck containers content
    LookupsTest(v);
    LookupsTest(copy);

    ASSERT_EQ(v[0].second, "five");
    ASSERT_EQ(v[0].first, 5);
    ASSERT_EQ(v[1].second, "zero");
    ASSERT_EQ(v[1].first, 0);

    ASSERT_EQ(copy[0].second, "five");
    ASSERT_EQ(copy[0].first, 5);
    ASSERT_EQ(copy[1].second, "zero");
    ASSERT_EQ(copy[1].first, 0);
    ASSERT_EQ(copy[2].first, 88);
    ASSERT_EQ(copy[2].second, "88");
}